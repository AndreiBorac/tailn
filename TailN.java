/***
 * TailN.java
 * copyright (c) 2018 by andrei borac
 ***/

import java.io.*;

public class TailN
{
  static final int DEFAULT_BUFFER = 65536;
  
  static class ByteQueue
  {
    byte[] arr;
    int    pos;
    int    lim;
    
    ByteQueue()
    {
      arr = (new byte[DEFAULT_BUFFER]);
      pos = 0;
      lim = 0;
    }
    
    int len()
    {
      return (lim - pos);
    }
    
    byte get(int idx)
    {
      if (!((pos + idx) < lim)) throw null;
      
      return arr[(pos + idx)];
    }
    
    byte removeFirst()
    {
      if (!(pos < lim)) throw null;
      
      //System.err.println("removeFirst '" + ((char)(arr[pos])) + "'");
      
      return arr[pos++];
    }
    
    private void replaceWith(byte[] neo)
    {
      for (int i = pos; i < lim; i++) {
        neo[(i - pos)] = arr[i];
      }
      
      arr = neo;
      lim -= pos;
      pos = 0;
    }
    
    void appendLast(byte val)
    {
      //System.err.println("appendLast '" + ((char)(val)) + "'");
      
      if (lim < arr.length) {
        arr[lim++] = val;
      } else {
        if (pos > (arr.length / 2)) {
          replaceWith(arr);
        } else {
          replaceWith((new byte[(arr.length * 2)]));
        }
        
        appendLast(val);
      }
    }
    
    void drainTo(BufferedOutputStream out) throws IOException
    {
      out.write(arr, pos, (lim - pos));
    }
  }
  
  public static void main(String[] args) throws Exception
  {
    final int N = Integer.parseInt(args[0]);
    
    //System.err.println("N=" + N);
    
    final ByteQueue bq = (new ByteQueue());
    
    final BufferedInputStream inp =
      (new BufferedInputStream
       (System.in));
    
    // linec represents the number of complete lines (with newline terminator) that are in bq
    int linec = 0;
    
    while (true) {
      byte[] buf = (new byte[DEFAULT_BUFFER]);
      
      int amt = inp.read(buf, 0, buf.length);
      
      if (amt < 0) break;
      
      // process each input byte separately
      for (int i = 0; i < amt; i++) {
        // always append the byte to bq
        bq.appendLast(buf[i]);
        
        if (buf[i] == ((byte)('\n'))) {
          // if the byte was a newline, increment the line counter
          linec++;
          
          // if we have too many lines, eliminate one (there won't be a need to eliminate more than one)
          if (linec > N) {
            while (bq.removeFirst() != ((byte)('\n')));
            linec--;
          }
        }
      }
    }
    
    // if we have exactly the requested number of lines in bq, and bq ends with a partial line (no newline),
    // then we need to purge one line from the head of bq (since the partial line counts as a line)
    if ((linec == N) && (bq.get((bq.len() - 1)) != ((byte)('\n')))) {
      while ((bq.len() > 0) && (bq.removeFirst() != ((byte)('\n'))));
    }
    
    final BufferedOutputStream out =
      (new BufferedOutputStream
       (System.out));
    
    bq.drainTo(out);
    
    out.flush();
  }
}
