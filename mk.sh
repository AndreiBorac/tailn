#!/usr/bin/env bash

set -o xtrace
set -o errexit
set -o nounset
set -o pipefail

PROJECT_UUID=32e53eaa-8e3b-4eb5-8ef6-d03a719a2899

mkdir -m 0700 -p /tmp/"$PROJECT_UUID"

javac -d /tmp/"$PROJECT_UUID" *.java

cd /tmp/"$PROJECT_UUID"

for i in `seq 1 100`
do
  dd if=/dev/urandom of=./rng count="$(( ((RANDOM*100) + RANDOM) ))" iflag=count_bytes
  
  N="$(( (RANDOM % 100) ))"
  
  if [ "$( cat ./rng | tail -n "$N" | sha256sum - )" != "$( cat ./rng | java -cp . TailN "$N" | sha256sum - )" ]
  then
    echo "test failed"
    exit 1
  fi
done

echo "+OK (all tests passed)"
